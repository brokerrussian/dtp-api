const justMongo = require('just-mongo');
const config = require('config');

const mongo = new justMongo(config.get('mongo'));

module.exports = mongo;