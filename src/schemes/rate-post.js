const Schema = require('../../config/system/schema');

module.exports = {
  "type": "object",
  "properties": {
    "shortId": Schema.short_id,
    "delta": Schema.voteDelta
  },
  "required": ["shortId", "delta"]
};