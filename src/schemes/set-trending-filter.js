const Schema = require('../../config/system/schema');

module.exports = {
  "type": "object",
  "properties": {
    "language": Schema.languages,
    "tags": {
      "type": "array",
      "items": Schema.tag,
      "minItems": 0,
      "maxItems": 10
    }
  },
  "required": ["language", "tags"]
};