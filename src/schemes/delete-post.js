const Schema = require('../../config/system/schema');

module.exports = {
  "type": "object",
  "properties": {
    "controlKey": Schema.uuid,
    "account_id": Schema.uuid
  },
  "required": ["controlKey"]
};