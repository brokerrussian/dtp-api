const Schema = require('../../config/system/schema');

module.exports = {
  "type": "object",
  "properties": {
    "user_id": Schema.user_id,
  },
  "required": ["user_id"]
};