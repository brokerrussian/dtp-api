const $Users = require('../modules/users');
const $log = require('../libs/log');
const $validtor = require('jsonschema').validate;

const moduleName = 'handlers.edit-account';

const ERRORS = require('../references/errors');
const authCheck = require('../middlewares/auth');
const accountAccessCheck = require('../middlewares/account-access');
const SCHEMA = require('../schemes/create-account');

const _ = require('lodash');

module.exports = function (app) {
  app.post('/editAccount/:account_id', authCheck, accountAccessCheck, (req, res) => {
    const valid = $validtor(req.body, SCHEMA);

    if (valid.errors.length > 0) {
      return res.json({
        ok: false,
        error: {
          msg: 'invalid params',
          data: valid.errors
        }
      });
    }

    const userId = req.session.user.id;
    const accountId = req.params.account_id;

    $Users.editAccount(userId, accountId, req.body)
      .then(async (result) => {
        res.json({
          ok: true
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s]', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};