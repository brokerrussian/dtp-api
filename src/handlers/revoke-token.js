const $validtor = require('jsonschema').validate;
const $Users = require('../modules/users');
const $log = require('../libs/log');
const moduleName = 'handlers.revoke-token';

const _ = require('lodash');

const ERRORS = require('../references/errors');
const authHandler = require('../middlewares/auth');

module.exports = function (app) {
  app.post('/revokeToken', authHandler, (req, res) => {
    $Users.revokeSecretToken(req.session.user.id)
      .then((newToken) => {
        res.json({
          ok: true,
          result: newToken
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s]', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};