const $Users = require('../modules/users');
const $log = require('../libs/log');

const moduleName = 'handlers.get-trending-filter';

const ERRORS = require('../references/errors');
const authCheck = require('../middlewares/auth');

const _ = require('lodash');

module.exports = function (app) {
  app.post('/getTrendingFilter', authCheck, (req, res) => {
    $Users.getTrendingFilter(req.session.user.id)
      .then(async (result) => {
        res.json({
          ok: true,
          result
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s]', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};