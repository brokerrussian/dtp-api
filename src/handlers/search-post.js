const $Posts = require('../modules/posts');
const $TelegraphAPI = require('../modules/telegraph-api');
const $log = require('../libs/log');
const $validtor = require('jsonschema').validate;
const moduleName = 'handlers.search-post';

const ERRORS = require('../references/errors');
const SCHEMA = require('../schemes/search-post');

const _ = require('lodash');

module.exports = function (app) {
  app.post('/searchPost', (req, res) => {
    const valid = $validtor(req.body, SCHEMA);

    if (valid.errors.length > 0) {
      return res.json({
        ok: false,
        error: {
          msg: 'invalid params',
          data: valid.errors
        }
      });
    }

    $Posts.searchPost(req.body)
      .then(async (result) => {
        res.json({
          ok: true,
          result
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s] error from search post', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};