const $validtor = require('jsonschema').validate;
const $Posts = require('../modules/posts');
const $log = require('../libs/log');
const moduleName = 'handlers.delete-post';

const _ = require('lodash');

const SCHEMA = require('../schemes/delete-post');
const ERRORS = require('../references/errors');
const accountAccessCheck = require('../middlewares/account-access');
const authHandler = require('../middlewares/auth');

module.exports = function (app) {
  app.post('/deletePost', authHandler, (req, res, next) => {
    req.skipEmptyAccountId = true;
    next();
  }, accountAccessCheck, (req, res) => {
    const valid = $validtor(req.body, SCHEMA);

    if (valid.errors.length > 0) {
      return res.json({
        ok: false,
        error: {
          msg: 'invalid params',
          data: valid.errors
        }
      });
    }

    const userId = req.session.user.id;
    let accountId = req.body.account_id || userId;

    $Posts.deleteByControlKey(accountId, req.body.controlKey)
      .then((result) => {
        res.json({
          ok: true
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s] error from create post', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};