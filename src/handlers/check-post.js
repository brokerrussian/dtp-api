const $Posts = require('../modules/posts');
const $log = require('../libs/log');
const moduleName = 'handlers.check-post';

const _ = require('lodash');

const ERRORS = require('../references/errors');
const authHandler = require('../middlewares/auth');
const accountAccessCheck = require('../middlewares/account-access');

module.exports = function (app) {
  app.post('/checkPost/:path', (req, res, next) => {
    req.allowEmptyToken = true;
    req.skipEmptyAccountId = true;
    next();
  }, authHandler, accountAccessCheck, (req, res) => {
    $Posts.check(req.params.path, req.session)
      .then((result) => {
        res.json({
          ok: true,
          result
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s]', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};
