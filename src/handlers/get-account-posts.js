const $Posts = require('../modules/posts');
const $log = require('../libs/log');
const $validtor = require('jsonschema').validate;

const moduleName = 'handlers.get-account-posts';

const ERRORS = require('../references/errors');
const authCheck = require('../middlewares/auth');
const accountAccessCheck = require('../middlewares/account-access');
const SCHEMA = require('../schemes/get-account-posts');

const _ = require('lodash');

module.exports = function (app) {
  app.post('/getPostsByAccount/*', authCheck, accountAccessCheck, (req, res) => {
    const valid = $validtor(req.body, SCHEMA);

    if (valid.errors.length > 0) {
      return res.json({
        ok: false,
        error: {
          msg: 'invalid params',
          data: valid.errors
        }
      });
    }

    $Posts.getListByAccount(req.session.groupAccount.id, req.body)
      .then(async (result) => {
        res.json({
          ok: true,
          result
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s] error from get account posts', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};