const $Users = require('../modules/users');
const $log = require('../libs/log');
const moduleName = 'handlers.get-token';

const ERRORS = require('../references/errors');

const _ = require('lodash');

module.exports = function (app) {
  app.post('/getToken/*', (req, res) => {
    const openToken = req.params[0];

    $Users.getSecretByOpen(openToken)
      .then(async (response) => {
        if (!response) {
          return res.json({
            ok: false,
            error: ERRORS['invalid-open-token']
          });
        }

        await $Users.revokeOpenToken(response.id);

        res.json({
          ok: true,
          result: response
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s] error from get token', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};