const $Users = require('../modules/users');
const $log = require('../libs/log');
const $validtor = require('jsonschema').validate;

const moduleName = 'handlers.create-account';

const ERRORS = require('../references/errors');
const authCheck = require('../middlewares/auth');
const SCHEMA = require('../schemes/create-account');

const _ = require('lodash');

module.exports = function (app) {
  app.post('/createAccount', authCheck, (req, res) => {
    if (req.session.user.ban) {
      return res.json({
        ok: false,
        error: ERRORS['user-ban']
      });
    }
    
    const valid = $validtor(req.body, SCHEMA);

    if (valid.errors.length > 0) {
      return res.json({
        ok: false,
        error: {
          msg: 'invalid params',
          data: valid.errors
        }
      });
    }

    $Users.createAccount(req.session.user.id, req.body)
      .then(async (result) => {
        res.json({
          ok: true
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s]', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};