const $Posts = require('../modules/posts');
const $TelegraphAPI = require('../modules/telegraph-api');
const $log = require('../libs/log');
const moduleName = 'handlers.get-post';

const ERRORS = require('../references/errors');

const _ = require('lodash');

module.exports = function (app) {
  app.post('/getPost/*', (req, res) => {
    const shortId = req.params[0];
    const returnContent = Boolean(_.get(req, 'body.return_content', true));

    $Posts.getByShortId(shortId)
      .then(async (post) => {
        if (!post) {
          return res.json({
            ok: false,
            error: ERRORS['post-source-invalid']
          });
        }

        let pContent = [];

        if (returnContent) {
          pContent = await $Posts.getPostContent(post.path);
        }
    
        res.json({
          ok: true,
          result: {
            shortId,
            preview: post.preview,
            language: post.language,
            tags: post.tags,
            content: pContent,
            votes: post.votes,
            date: post.createdDate
          }
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s] error from get post', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};