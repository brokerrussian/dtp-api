const moduleName = 'handlers.account-access';

const $Users = require('../modules/users');
const $log = require('../libs/log');

const _ = require('lodash');

const ERRORS = require('../references/errors');
const groupAccountAccess = require('../references/group-account-access');


module.exports = function (req, res, next) {
  const userId = _.get(req, 'session.user.id');
  const accountId = req.params[0] || req.params.account_id || req.body.account_id || false;

  if (!accountId.length && !req.skipEmptyAccountId) {
    return res.json({
      ok: false,
      error: ERRORS['invalid-account-id']
    });
  } else if (!accountId.length && req.skipEmptyAccountId) {
    return next();
  }

  $Users.checkAccessToAccount(userId, accountId)
    .then(async (access) => {
      if (access === groupAccountAccess['no']) {
        return res.json({
          ok: false,
          error: ERRORS['denied-account']
        });
      }

      _.set(req, 'session.groupAccount.access', access);
      _.set(req, 'session.groupAccount.id', accountId);

      next();
    })
    .catch((err) => {
      if (!_.get(err, 'Error')) {
        $log.error('[%s] error from ', moduleName, err);
      }

      res.json({
        ok: false,
        error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
      });
    });
};
