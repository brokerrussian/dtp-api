const $request = require('request');
const $Promise = require('bluebird');
const $config = require('config');

class Language {
  static get SERVER_URL () {
    return 'https://translate.yandex.net/api/v1.5/tr.json/detect';
  }

  static get METHOD () {
    return 'GET';
  }

  static get API_KEY () {
    return $config.get('services.yandex.apiKey');
  }

  static async getCodeHTTP (text) {
    const languages = ['en', 'ru', 'uk', 'fr', 'it', 'es', 'nl'];
    const shortText = text.split('.').reduce((res, part) => {
      if (res.length > 120) {
        return res;
      }

      res += part;

      return res;
    }, '');
    const qs = {
      key: Language.API_KEY,
      text: shortText
    };

    const result = await new $Promise((resolve, reject) => {
      $request({
        method: Language.METHOD,
        url: Language.SERVER_URL,
        qs
      }, (err, response, body) => {
        if (err) {
          return reject(err);
        }

        let jn;

        try {
          jn = JSON.parse(body);
        } catch (err) {
          return resolve('errror');
        }

        resolve(jn.lang || 'error');
      })
    });

    if (!languages.includes(result)) {
      return 'other';
    }

    return result;
  }

  static async getCode (text) {
    return await $Promise.race([
      Language.getCodeHTTP(text),
      new $Promise((resolve, reject) => {
        setTimeout(() => {
          resolve('other');
        }, 2500);
      })
    ]);
  }
}

module.exports = Language;

if (process.env.TEST) {
  (async () => {
    console.log(await Language.getCode('3453'));
  })();
}
