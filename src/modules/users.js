const $mongo = require('../libs/mongo');
const $AF = require('../libs/action-flow');
const $uuid = require('uuid/v4');
const $sha256 = require('sha256');
const $TelegraphAPI = require('../modules/telegraph-api');
const $Promise = require('bluebird');
const $config = require('config');
const _ = require('lodash');

const groupAccountAccess = require('../references/group-account-access');
const ERRORS = require('../references/errors');

const ACCOUNTS_LIMIT = 10;
const MEMEBERS_LIMIT = 20;

class Users {
  static get db () {
    return $mongo.collection('users');
  }

  static get groupAccountsMembersDB () {
    return $mongo.collection('groupAccountsMembers');
  }

  static get groupAccountsDB () {
    return $mongo.collection('groupAccounts');
  }

  static get groupAccountInvitesDB () {
    return $mongo.collection('groupAccountsInvites');
  }

  static get trendingFiltersDB () {
    return $mongo.collection('trendingFilters');
  }

  static get postsDB () {
    return $mongo.collection('posts');
  }

  static get telegraphTokensDB () {
    return $mongo.collection('telegraphTokens');
  }

  static get authorNamesSandboxDB () {
    return $mongo.collection('authorNamesSandbox');
  }

  static get contentBlacklistDB () {
    return $mongo.collection('contentBlacklist');
  }

  static async setSession (id) {
    const sessionTime = Math.round((new Date()).getTime() / 1000);

    await Users.db.editOne({ id }, {
      last_api_session: sessionTime
    });

    return true;
  }

  static async getById (id) {
    return await Users.db.findOne({ id });
  }

  static async getBySecretToken (token) {
    return await Users.db.findOne({ secretToken: token });
  }

  static async create (userObject) {
    const user = _.pick(userObject, [
      'id', 'first_name', 'last_name'
    ]);

    user.openToken = $sha256($uuid());
    user.secretToken = $sha256($uuid());

    return await Users.db.insert(user);
  }

  static async getToken(id) {
    const user = await Users.getById(id);
    let token = _.get(user, 'secretToken');

    if (!token) {
      token = $sha256($uuid());

      await Users.db.editOne({ id }, {
        secretToken: token
      });
    }

    return token;
  }

  static async revokeOpenToken (id) {
    const newToken = $sha256($uuid());

    await Users.db.editOne({ id }, {
      openToken: newToken
    });

    return newToken;
  }

  static async revokeSecretToken (id) {
    const newToken = $sha256($uuid());

    await Users.db.editOne({ id }, {
      secretToken: newToken
    });

    return newToken;
  }

  static async getSecretByOpen (openToken) {
    const user = await Users.db.findOne({ openToken });

    if (!user) {
      return;
    }

    if (!user.secretToken) {
      const newSecret = $sha256($uuid());
      await Users.db.editOne({ _id: user._id }, {
        secretToken: newSecret
      });

      user.secretToken = newSecret;
    }

    return _.pick(user, ['secretToken', 'first_name', 'last_name', 'id', 'photo']);
  }

  static async checkAccessToAccount (userId, accountId) {
    const accountIsUser = !!Number(accountId);

    if (accountIsUser) {
      if (Number(accountId) === userId) {
        return groupAccountAccess['owner'];
      } else {
        return groupAccountAccess['no'];
      }
    }

    const member = await Users.groupAccountsMembersDB.findOne({
      user_id: userId,
      account_id: accountId
    });

    if (!member) {
      return groupAccountAccess['no'];
    }

    return member.access;
  }

  static async getAccounts (userId) {
    const accounts = await Users.groupAccountsMembersDB.join({
      user_id: userId
    }, Users.groupAccountsDB, {
      account_id: 'id'
    }, {
      id: '1.id',
      short_name: '1.name',
      access: '0.access'
    });

    return accounts;
  }

  static async transferAccount (userId, token) {
    const exists = await Users.groupAccountsDB.findOne({
      token
    });

    if (exists) {
      throw {Error: ERRORS['denied-account']}
    }

    const telegraphAccount = await $TelegraphAPI.query('getAccountInfo', {
      access_token: token,
      fields: ['short_name']
    })
      .then((data) => {
        if (!data.ok) {
          throw {Error: ERRORS['denied-account']}
        }

        return data.result;
      });

    const accountId = $uuid();
    const createdDate = Math.round((new Date()).getTime() / 1000);

    await Users.groupAccountsDB.insert({
      id: accountId,
      name: telegraphAccount.short_name,
      createdDate,
      token
    });


    await Users.groupAccountsMembersDB.insert({
      user_id: userId,
      account_id: accountId,
      access: groupAccountAccess['owner']
    });
  }

  static async createAccount (userId, data) {
    const accountCount = await Users.groupAccountsMembersDB.count({
      user_id: userId
    });

    if (accountCount >= ACCOUNTS_LIMIT) {
      throw {Error: ERRORS['too-many-accounts']};
    }

    if (data.transfer) {
      return Users.transferAccount(userId, data.transfer);
    }

    let username;

    if (data.author_url) {
      username = data.author_url.split(`${$config.get('services.shortHost.url')}/`)[1];
    }

    if (username) {
      username = username.toLowerCase();

      const check = await Users.checkAuthorUsername(userId, username);

      if (check.exists) {
        throw {Error: ERRORS['username-reserved']};
      }
    }

    const telegraphAccount = await $TelegraphAPI.query('createAccount', {
      short_name: data.short_name,
      author_name: data.author_name,
      author_url: data.author_url
    })
      .then((data) => {
        if (!data.ok) {
          throw data;
        }

        return data.result;
      });

    const accountToken = telegraphAccount.access_token;
    const accountId = $uuid();
    const createdDate = Math.round((new Date()).getTime() / 1000);

    await Users.createTelegraphToken(userId, accountId, accountToken);

    const insertData = {
      id: accountId,
      name: data.short_name,
      createdDate,
      token: accountToken
    };

    if (username) {
      insertData.username = username;
    }

    await Users.groupAccountsDB.insert(insertData);
    await Users.groupAccountsMembersDB.insert({
      user_id: userId,
      account_id: accountId,
      access: groupAccountAccess['owner']
    });
  }

  static async deleteAccount (userId, accountId) {
    const member = await Users.groupAccountsMembersDB.findOne({
      account_id: accountId,
      user_id: userId
    });

    if (!member) {
      return;
    }

    if (member.access === groupAccountAccess['owner']) {
      await Users.revokeTokenAccount(userId, accountId);
      await $Promise.all([
        Users.groupAccountsDB.deleteMany({
          id: accountId
        }),
        Users.groupAccountsMembersDB.deleteMany({
          account_id: accountId
        }),
        Users.movePostsPermissions(accountId, userId),
        Users.groupAccountInvitesDB.deleteMany({
          account_id: accountId
        })
      ]);
    } else {
      await $Promise.all([
        Users.groupAccountsMembersDB.deleteMany({
          user_id: userId,
          account_id: accountId
        }),
        Users.revokeTokenAccount(userId, accountId)
      ]);
    }
  }

  static async movePostsPermissions (fromId, toId) {
    const filterFrom = {};
    const dataTo = {};

    if (typeof fromId === 'number') {
      filterFrom.user_id = fromId;
    } else {
      filterFrom.account_id = fromId;
    }

    if (typeof toId === 'number') {
      dataTo.$unset = {account_id: ''};
      dataTo.$set = {user_id: toId}
    } else {
      dataTo.$unset = {user_id: ''};
      dataTo.$set = {account_id: toId}
    }

    await Users.postsDB.updateMany(filterFrom, dataTo);
  }

  static async revokeTokenAccount (userId, accountId) {
    const account = await Users.groupAccountsDB.findOne({
      id: accountId
    });

    const token = account.token;

    const newToken = await $TelegraphAPI.query('revokeAccessToken', {
      access_token: token
    })
      .then((data) => {
        if (!data.ok) {
          if (data.error !== 'ACCESS_TOKEN_INVALID') {
            throw data;
          } else {
            return 'ACCESS_TOKEN_INVALID';
          }
        }

        return data.result.access_token;
      });

    if (newToken === 'ACCESS_TOKEN_INVALID') {
      return;
    }

    await Users.createTelegraphToken(userId, accountId, newToken);

    await Users.groupAccountsDB.editOne({
      _id: account._id
    }, {
      token: newToken
    });
  }

  static async checkAuthorUsername (userId, username) {
    username = username.toLowerCase();
    
    const { account, isBan, isPost } = await $Promise.props({
      account: Users.groupAccountsDB.count({
        username
      }),
      isBan: Users.contentBlacklistDB.count({
        type: 'username',
        value: username
      }),
      isPost: Users.postsDB.count({
        shortId: username
      })
    });

    if (account || isBan || isPost) {
      return {exists: true};
    }

    const expired = Math.round( (new Date()) / 1000 ) - 60;
    const inSandbox = await Users.authorNamesSandboxDB.findOne({
      username,
      createdDate: {$gt: expired}
    });

    if (inSandbox && inSandbox.user_id !== userId) {
      return {exists: true};
    } else if (inSandbox && inSandbox.user_id === userId) {
      return {exists: false, reserved: true};
    }
    
    return {exists: false};
  }

  static async reserveAuthorUsername (userId, username) {
    username = username.toLowerCase();

    const check = await Users.checkAuthorUsername(userId, username);

    if (check.exists) {
      throw {Error: ERRORS['username-reserved']};
    } else if (check.reserved) {
      return true;
    }

    const createdDate = Math.round((new Date()).getTime() / 1000);
    
    await Users.authorNamesSandboxDB.insert({
      user_id: userId,
      username,
      createdDate
    });

    return true;
  }

  static async editAccount (userId, accountId, data) {
    const account = await Users.groupAccountsDB.findOne({
      id: accountId
    });

    if (!account) {
      throw {Error: ERRORS['invalid-account-id']}
    }

    const member = await Users.groupAccountsMembersDB.findOne({
      account_id: accountId,
      user_id: userId
    });

    if (!member) {
      throw {Error: ERRORS['denied-account']};
    }

    if (member.access !== groupAccountAccess['owner']) {
      throw {Error: ERRORS['denied-account']};
    }

    const accountToken = account.token;
    let username;

    if (data.author_url) {
      username = data.author_url.split(`${$config.get('services.shortHost.url')}/`)[1];

      if (username) {
        username = username.toLowerCase();
      }
    }

    if (username && username !== account.username) {
      const check = await Users.checkAuthorUsername(userId, username);

      if (check.exists) {
        throw {Error: ERRORS['username-reserved']};
      }
    } else {
      username = false;
    }

    await $TelegraphAPI.query('editAccountInfo', {
      access_token: accountToken,
      short_name: data.short_name,
      author_name: data.author_name,
      author_url: data.author_url
    })
      .then(async (data) => {
        if (!data.ok) {
          if (data.error === 'ACCESS_TOKEN_INVALID') {
            await Users.deleteAccount(userId, accountId);
            throw {Error: ERRORS['denied-account']};
          }

          throw data;
        }
      });

    const updateData = {
      name: data.short_name
    };

    if (username) {
      updateData.username = username;
    }

    if (!account.createdDate) {
      const createdDate = Math.round((new Date()).getTime() / 1000);
      updateData.createdDate = createdDate;
    }

    await Users.groupAccountsDB.editOne({
      id: accountId
    }, updateData);
  }

  static async getAuthor (username) {
    username = username.toLowerCase();
    
    const account = await Users.groupAccountsDB.findOne({
      username
    });

    if (!account) {
      throw {Error: ERRORS['invalid-account-id']};
    }

    const token = account.token;
    const info = await $TelegraphAPI.query('getAccountInfo', {
      access_token: token,
      fields: ['author_name']
    })
      .then((data) => {
        if (!data.ok) {
          if (data.error === 'ACCESS_TOKEN_INVALID') {
            throw {Error: ERRORS['denied-account']};
          }

          throw data;
        }

        return data.result;
      });

    return {
      name: info.author_name,
      username
    };
  }

  static async getAccount (accountId) {
    const account = await Users.groupAccountsDB.findOne({
      id: accountId
    });

    if (!account) {
      throw {Error: ERRORS['invalid-account-id']}
    }

    const accountToken = account.token;
    const telegraphAccount = await $TelegraphAPI.query('getAccountInfo', {
      access_token: accountToken,
      fields: ['short_name', 'author_name', 'author_url', 'auth_url', 'page_count']
    })
      .then(async (data) => {
        if (!data.ok) {
          if (data.error === 'ACCESS_TOKEN_INVALID') {
            throw {Error: ERRORS['denied-account']};
          }

          throw data;
        }

        return data.result;
      });

    const accInfo = {
      id: accountId
    };

    Object.assign(accInfo, telegraphAccount);

    return accInfo;
  }

  static async getAccountInvite (accountId) {
    const invite = await Users.groupAccountInvitesDB.findOne({
      account_id: accountId
    });
    const newId = $uuid();

    if (!invite) {
      await Users.groupAccountInvitesDB.insert({
        id: newId,
        account_id: accountId
      });
    } else {
      await Users.groupAccountInvitesDB.editOne({
        account_id: accountId
      }, {
        id: newId
      });
    }

    const publisherBot = $config.get('services.publisherBot.username');
    const inviteLink = `https://t.me/${publisherBot}?start=gin${newId}`;

    return inviteLink;
  }

  static async getAccountMembers (accountId) {
    const members = await Users.groupAccountsMembersDB.join({
      account_id: accountId
    }, Users.db, {
      user_id: 'id'
    }, {
      id: '1.id',
      first_name: '1.first_name',
      last_name: '1.last_name',
      photo: '1.photo',
      access: '0.access'
    });

    return members;
  }

  static async createTelegraphToken (userId, accountId, token) {
    const createdDate = Math.round((new Date()).getTime() / 1000);
    
    await Users.telegraphTokensDB.insert({
      user_id: userId,
      account_id: accountId,
      token,
      createdDate
    });
  }

  static async inviteAccountMember (userId, inviteId) {
    const invite = await Users.groupAccountInvitesDB.findOne({
      id: inviteId
    });

    if (!invite) {
      throw {Error: ERRORS['invalid-invite']};
    }

    const countMembersOfAccount = await Users.groupAccountsMembersDB.count({
      account_id: invite.account_id
    });

    if (countMembersOfAccount >= MEMEBERS_LIMIT) {
      throw {Error: ERRORS['too-many-members']};
    }

    const countAccountsOfUser = await Users.groupAccountsMembersDB.count({
      user_id: userId
    });

    if (countAccountsOfUser >= ACCOUNTS_LIMIT) {
      throw {Error: ERRORS['too-many-accounts']};
    }

    const exists = await Users.groupAccountsMembersDB.count({
      user_id: userId,
      account_id: invite.account_id
    });

    if (exists) {
      return;
    } else {
      await Users.groupAccountsMembersDB.insert({
        user_id: userId,
        account_id: invite.account_id,
        access: groupAccountAccess['editor']
      });
    }
  }

  static async getTrendingFilter (userId) {
    const defaultFilter = {
      language: [],
      tags: []
    };

    const filter = await Users.trendingFiltersDB.findOne({
      user_id: Number(userId)
    });

    if (!filter) {
      return defaultFilter;
    }

    return _.pick(filter, ['language', 'tags']);
  }

  static async setTrendingFilter (userId, data) {
    const exists = await Users.trendingFiltersDB.count({
      user_id: Number(userId)
    });

    if (exists !== 0) {
      await Users.trendingFiltersDB.editOne({
        user_id: Number(userId)
      }, {
        language: data.language,
        tags: data.tags
      });
    } else {
      await Users.trendingFiltersDB.insert({
        user_id: Number(userId),
        language: data.language,
        tags: data.tags
      });
    }
  }
}

module.exports = Users;
